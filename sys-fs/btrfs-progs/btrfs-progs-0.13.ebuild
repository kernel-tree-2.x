# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils 

DESCRIPTION="btrfs filesystem utilities"
HOMEPAGE="http://oss.oracle.com/projects/btrfs/"
SRC_URI="http://oss.oracle.com/projects/btrfs/dist/files/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${RDEPEND}
		sys-fs/e2fsprogs"

src_unpack() {
	unpack ${A}
	cd "${S}"
	# Change installation directory to a more suitable place
	epatch "${FILESDIR}"/${P}-makefile.patch
}

src_compile() {
	emake || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "install failed"
}

pkg_postinst() {
	einfo
	einfo "The Btrfs disk format is not yet finalized and it currently does not"
	einfo "handle disk full conditions at all. Things are under heavy development" 
	einfo "and Btrfs is not suitable for any uses other than benchmarking and review."
	einfo
	einfo "Documentation and the kernel module can be found at."
	einfo "http://oss.oracle.com/projects/btrfs"
	einfo
}
