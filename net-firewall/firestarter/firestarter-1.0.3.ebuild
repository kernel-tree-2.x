# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="GUI iptables firewalling utility"
HOMEPAGE="http://www.fs-security.com/"
SRC_URI="http://internap.dl.sourceforge.net/sourceforge/firestarter/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="pic"

DEPEND="net-firewall/iptables"
RDEPEND="${DEPEND}
		gnome-base/libgnomeui
		gnome-base/libgnome
		x11-libs/gtk+
		gnome-base/gnome-vfs
		gnome-base/libglade"

src_unpack() {
	unpack ${A}
	cd "${S}"
}

src_compile() {
	econf \
		$(use_enable pic) \
		|| die "econf failed"
	emake || die "emake faild"
}

src_install() {
	mkdir -p "/etc/gconf/schemas/apps/firestarter"
	einstall || die "make install failed"

	dodoc AUTHORS CREDITS NEWS README TODO || die "dodoc failed"
}
